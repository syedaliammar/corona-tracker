

    export class Location {
        LAT: string;
        LONG: string;
    }

    export class Answer {
        questionid: string;
        answer: string[]
    }
    export class RootObject {
        DeviceID: string;
        DeviceType: string;
        Age: string;
        Gender: string;
        NumberFamilyMembersInHouse: string;
        PLZ: string;
        Location: Location;
        SubmitDatetime: String;
        Survey: Answer[];
    }



