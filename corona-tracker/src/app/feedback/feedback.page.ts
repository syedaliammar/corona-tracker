import { Component, OnInit } from '@angular/core';
import { AppConstants } from '../app.constants';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  feedbackSummary: string;
  constructor() { 
    
  }

  ngOnInit() {
    this.feedbackSummary = AppConstants.feedback;
  }

}
