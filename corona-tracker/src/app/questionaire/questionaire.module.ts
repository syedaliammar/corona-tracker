import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { QuestionairePage } from './questionaire.page';
import { HttpClientModule } from '@angular/common/http';
import { QuestionaireService } from '../services/questionaire.service';

const routes: Routes = [
  {
    path: '',
    component: QuestionairePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    QuestionaireService
  ],
  declarations: [QuestionairePage]
})
export class QuestionairePageModule {}
