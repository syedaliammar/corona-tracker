import { Component, OnInit } from '@angular/core';
import { QuestionaireService } from '../services/questionaire.service';
import { AppConstants } from '../app.constants';
import { ActivatedRoute, Router } from '@angular/router';
import { FormtransferService } from '../services/formtransfer.service';
import { AccountService } from '../services/account.service';
import { Answer } from '../model/RootObject';

@Component({
  selector: 'app-questionaire',
  templateUrl: './questionaire.page.html',
  styleUrls: ['./questionaire.page.scss'],
})
export class QuestionairePage implements OnInit {
  toastCtrl: any;
  account: string;
  questions : any;
   constructor(private route: ActivatedRoute, 
    private router: Router,
    private questionaireService: QuestionaireService, 
    private formtransfer: FormtransferService,
    private accountService: AccountService) { 
      
    }

  ngOnInit() {
      this.questions = AppConstants.questions;
  }
  
  
  SendQuestionaire(){
    // Save Data to Global json variable im AppConstants
    // to be done
    var answers : Answer[] = [];
    this.questions.forEach(element => {  
      var test = this.GetDataFromDom(element._id, element.answerType);
      answers.push(test);
    });
    AppConstants.answersJson.Survey = answers;
    this.router.navigateByUrl('/tabs/finish');    
  }
  GetDataFromDom(id: any, type: any): Answer {
    let item: Answer = new Answer();    
    item.questionid= id;
    var control = document.getElementById(id);
    switch(type){
      case "MULTICHOICE":
        var choices = control.getElementsByTagName("ion-checkbox");
        for(var i = 0; i< choices.length; i++) {
          if(choices[i].checked) {
            item.answer.push(choices[i].id);
          }
        }
        break;
      case "SINGLECHOICE":
        var radio = control.getElementsByTagName("ion-radio");
        for(var i = 0; i< radio.length; i++) {
          if(radio[i].checked) {
            var answer: string [] = [radio[i].id]
            item.answer =  answer;
            return item; 
          }
        }
        break;
      case "FREETEXT":
        item.answer.push(control.nodeValue);
        break;
    }
    return item;
  }
}
