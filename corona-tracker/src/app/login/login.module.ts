import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './login.page';
import { AuthenticationmailService } from '../services/authenticationmail.service';
import { QuestionaireService } from '../services/questionaire.service';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,    
    RouterModule.forChild(routes)
  ],
  providers: [
    AuthenticationmailService, QuestionaireService
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
