import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AppConstants } from './../app.constants';
import { QuestionaireService } from '../services/questionaire.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  login_email: string;
  constructor(private accountService: AccountService,    
    private questionaireService: QuestionaireService,  private router: Router, 
    private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  async validateEmail() {
     
        await this.GetQuestions().then(
          response =>  {
            AppConstants.questions = response.items;
            this.router.navigateByUrl('/tabs/questionaire')            
          }
        );                        
      
  }
  async GetQuestions(): Promise<any> {
    const res = await this.questionaireService.getQuestions().then((res => {       
       return res;
      })
    ).catch((resp => {
       if(resp.status ===404) {
        this.toastCtrl.create({
          message: 'Server not responding, Network Issues',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
           color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }
       if(resp.status ===400) {
        this.toastCtrl.create({
          message: 'Bad Request, App and server not in Sync. Please update the app',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
           color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }  
       if(resp.status ===200) {
        this.toastCtrl.create({
          message: 'User not Found in Valid-User Accounts',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }     
     }));
    return res;
  }
}
