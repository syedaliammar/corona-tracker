import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FinishPage } from './finish.page';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { SendToServerService } from '../services/sendtoserver.service';
import { HttpClientModule } from '@angular/common/http';
import { FeedbackService } from '../services/feedback.service';
const routes: Routes = [
  {
    path: '',
    component: FinishPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule, 
    RouterModule.forChild(routes)
  ],
  declarations: [FinishPage], 
  providers: [UniqueDeviceID, SendToServerService, FeedbackService]
})
export class FinishPageModule {}
