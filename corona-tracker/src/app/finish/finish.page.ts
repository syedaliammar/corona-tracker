import { Component, OnInit } from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { StorageService } from '../services/storage.service';
import { SendToServerService } from '../services/sendtoserver.service';
import { AppConstants } from '../app.constants';
import { Location } from '../model/RootObject';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FeedbackService } from '../services/feedback.service';
@Component({
  selector: 'app-finish',
  templateUrl: './finish.page.html',
  styleUrls: ['./finish.page.scss'],
})
export class FinishPage implements OnInit {
  finishAge: string;
  finishGender: string;
  finishPostalcode: string;
  finishIMEI: string;
  finishLatitude: string;
  finishLongitude: string;
  finishPeopleInHouse: string;

  constructor(private uniqueDeviceID: UniqueDeviceID, private storageService: StorageService ,
    private toastCtrl: ToastController,private router: Router, private sendToServerService: SendToServerService, 
    private feedbackService: FeedbackService) {
    this.uniqueDeviceID.get()
  .then((uuid: any) => {this.finishIMEI = uuid; this.storageService.storeImei(uuid); })
  .catch((error: any) => console.log(error));
   }

  ngOnInit() {
  }

  async SetStorage() {
     this.storageService.storeAge(this.finishAge);
     this.storageService.storeGender(this.finishGender);
     this.storageService.storePostalcode(this.finishPostalcode);     
     this.SendQuestionaire();
  }

  async SendQuestionaire() {
    this.setActivation(false);
    this.updateRootObject();
    var data = JSON.stringify(AppConstants.answersJson)
    this.postDataToServer(data).then((res => {
      this.toastCtrl.create({
        message: 'Data Posted',
        duration: AppConstants.toastTime,
        position: AppConstants.toastPosition,
        color: 'success'
      }).then(toast => {
        toast.present();
        this.getFeedBack(this.finishIMEI ? "":"ABCD_WIEUR24244").then((res =>{
          AppConstants.feedback = res.recommendation;
          this.router.navigateByUrl('/tabs/feedback')  
        }))
      });
    })      
    );
  }
  async getFeedBack(data: string) {
    const res = await this.feedbackService.GetFeedback(data).then((res => {
      return res;
     })
   ).catch((resp => {
      if(resp.status ===404) {
       this.toastCtrl.create({
         message: 'Server not responding, Network Issues',
         duration: AppConstants.toastTime,
         position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
       }).then(toast => toast.present());
      }
      if(resp.status ===400) {
       this.toastCtrl.create({
         message: 'Bad Request, App and server not in Sync. Please update the app',
         duration: AppConstants.toastTime,
         position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
       }).then(toast => toast.present());
      }  
      if(resp.status ===200) {
       this.toastCtrl.create({
         message: 'User not Found in Valid-User Accounts',
         duration: AppConstants.toastTime,
         position: AppConstants.toastPosition,
         color: AppConstants.toastFailureColor, // success
       }).then(toast => toast.present());
      }
      if(resp.status ===504) {
       this.toastCtrl.create({
         message: 'Server Settings issue',
         duration: AppConstants.toastTime,
         position: AppConstants.toastPosition,
         color: AppConstants.toastFailureColor, // success
       }).then(toast => toast.present());      
      } 
      if(resp.status ===204) {
       this.toastCtrl.create({
         message: 'Server Settings issue',
         duration: AppConstants.toastTime,
         position: AppConstants.toastPosition,
         color: AppConstants.toastFailureColor, // success
       }).then(toast => toast.present());
     }     
    }));       
   return res;
  }
  setActivation(activation: boolean) {
    var control = document.getElementById("submitFormButton");
    // control.disable = true;
  }
  updateRootObject() {
    AppConstants.answersJson.Age = this.finishAge;
    AppConstants.answersJson.DeviceID = this.finishIMEI;
    AppConstants.answersJson.Gender = this.finishGender;
    var location : Location = new Location();
    location.LAT = this.finishLatitude;
    location.LONG = this.finishLongitude;
    AppConstants.answersJson.Location = location;
    AppConstants.answersJson.NumberFamilyMembersInHouse = this.finishPeopleInHouse;
    AppConstants.answersJson.PLZ = this.finishPostalcode;
    let dateTime = new Date();
    AppConstants.answersJson.SubmitDatetime = dateTime.toISOString();
  }
async postDataToServer(data: string): Promise<any> {
    const res = await this.sendToServerService.postData(data).then((res => {
       return res;
      })
    ).catch((resp => {
       if(resp.status ===404) {
        this.toastCtrl.create({
          message: 'Server not responding, Network Issues',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
           color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }
       if(resp.status ===400) {
        this.toastCtrl.create({
          message: 'Bad Request, App and server not in Sync. Please update the app',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
           color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }  
       if(resp.status ===200) {
        this.toastCtrl.create({
          message: 'User not Found in Valid-User Accounts',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
       }
       if(resp.status ===504) {
        this.toastCtrl.create({
          message: 'Server Settings issue',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());      
       } 
       if(resp.status ===204) {
        this.toastCtrl.create({
          message: 'Server Settings issue',
          duration: AppConstants.toastTime,
          position: AppConstants.toastPosition,
          color: AppConstants.toastFailureColor, // success
        }).then(toast => toast.present());
      }     
     }));       
    return res.status;
  }
}
