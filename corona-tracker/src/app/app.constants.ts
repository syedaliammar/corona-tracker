import { RootObject } from './model/RootObject';

export class AppConstants {
  static toastTime: number = 3000;
  static toastPosition: any =  'bottom';
  static toastFailureColor: string = 'primary';
  static questions: any;
  static answersJson: RootObject = new RootObject();
  static feedback : any;
}