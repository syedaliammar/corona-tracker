import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'info', loadChildren: './info/info.module#InfoPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' }, 
  { path: 'questionaire', loadChildren: './questionaire/questionaire.module#QuestionairePageModule' },
  { path: 'finish', loadChildren: './finish/finish.module#FinishPageModule' },
  { path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
