import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'questionaire',
        children: [
          {
            path: '',
            loadChildren: '../questionaire/questionaire.module#QuestionairePageModule'
          }
        ]
      },
      {
        path: 'info',
        children: [
          {
            path: '',
            loadChildren: '../info/info.module#InfoPageModule'
          }
        ]
      },
      {
        path: 'finish',
        children: [
          {
            path: '',
            loadChildren: '../finish/finish.module#FinishPageModule'
          }
        ]
      },
      {
        path: 'feedback',
        children: [
          {
            path: '',
            loadChildren: '../feedback/feedback.module#FeedbackPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
