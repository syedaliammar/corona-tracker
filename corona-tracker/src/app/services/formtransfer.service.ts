import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class FormtransferService {

  myParams: any;

      constructor() { }

      public setFormParams(data) {
        this.myParams = data;
      }

      public getFormParams() {
        return this.myParams;
      }
    }
