import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuestionaireService {
  private apiUrl = 'https://corona-tracker.9dev.de/questions';
  
  constructor(private http: HttpClient) {}

  getQuestions(): Promise<any> {
    return this.http.get<any>(this.apiUrl).toPromise();
    
  }
}
