import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})


export class AccountService {

  public loaded = false;

  constructor(private storage: Storage) { }

      getAccount(): Promise<any> {
        return this.storage.get('corona-tracker_account');
      }

      storeAccount(data): void {
        this.storage.set('corona-tracker_account', data);
      }

}

