import { TestBed } from '@angular/core/testing';

import { FormtransferService } from './formtransfer.service';

describe('FormtransferService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormtransferService = TestBed.get(FormtransferService);
    expect(service).toBeTruthy();
  });
});
