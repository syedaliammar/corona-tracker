import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationmailService {
  private apiUrl = 'https://localhost:3000/authmail';
  private token = '';
  email: any;
  constructor(private http: HttpClient) {}

  authenticateMail(email): Promise<any> {
    var raw = JSON.stringify({"mail":email});
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    return this.http.post<any>(
          this.apiUrl, raw, {
            headers: httpHeaders                                
      }).toPromise();
  }
}
