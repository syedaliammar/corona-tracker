import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient, private storageService: StorageService) {

   }
private apiUrl = 'https://corona-tracker.9dev.de/entries/*uuid*/score'; // Replace *uuid* with IMEI from Storageservice later


    GetFeedback(IMEI: string): Promise<any> {
     return this.http.get<any>(this.apiUrl.replace('*uuid*', IMEI)).toPromise();
   }
}
