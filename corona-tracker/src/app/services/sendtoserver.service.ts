import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SendToServerService {
  private apiUrl = 'https://corona-tracker.9dev.de/entries';
  constructor(private http: HttpClient) {}
  postData(data): Promise<any> {    
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    return this.http.post<any>(
          this.apiUrl, data, {
            headers: httpHeaders                                
      }).toPromise();
  }
}
