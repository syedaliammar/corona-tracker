import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) {}

   getGender(): Promise<any> {
    return this.storage.get('corona-tracker_gender');
  }

  storeGender(data): void {
    this.storage.set('corona-tracker_gender', data);
  }

  getAge(): Promise<any> {
    return this.storage.get('corona-tracker_age');
  }

  storeAge(data): void {
    this.storage.set('corona-tracker_age', data);
  }

  getPostalcode(): Promise<any> {
    return this.storage.get('corona-tracker_postalcode');
  }

  storePostalcode(data): void {
    this.storage.set('corona-tracker_postalcode', data);
  }

  getImei(): Promise<any> {
    return this.storage.get('corona-tracker_imei');
  }

  storeImei(data): void {
    this.storage.set('corona-tracker_imei', data);
  }

  getPeopleInHouse(): Promise<any> {
    return this.storage.get('corona-tracker_people');
  }

  storePeopleInHouse(data): void {
    this.storage.set('corona-tracker_people', data);
  }


}
